---
name: faq
permalink: /faq/
title: "(supposedly) Frequetly Asked Questions"
layout: boxdrawn
no_sidebar: true
---

## FAQs about legacy blog
{% include legacy-blog width="400" height="50" %}

### Why “Mechdeveloper”?
 
I couldn’t find a proper domain for my website. Almost every domain based on my name (Miguel Rodríguez) were occupied by someone else or they were premium domains that were on sale for, at least, 1200$. Therefore, I decided to choose an original name that suit the website. I was thinking about something like “The Mechanical Developer”, but shrinking it I got “Mechdeveloper”.
 
### Are you a designer?, do you make images? (this was in the old website)
 
No and no, I consider myself a developer who tries to make things look good, I designed my own website, but, if I have to classify myself as designer, I would say that I am merely an amateur & enthusiast, all the images that I worked with were images on the web with Creative Common License, and I adapted it to my personal purpose, in case that I have to make something else like this, I would do the same, finding the proper images on the web with a convenient license and adapting it to the purpose I’m searching for, in case that I have a problem of this nature (images and design), I would ask for help to someone else.

