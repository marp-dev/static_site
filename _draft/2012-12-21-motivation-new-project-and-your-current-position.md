---
layout: buffers
slug: motivation-new-project-and-your-current-position
title: The motivation, the new project and your current position
date: 2012-12-21
draft: true
imgpath: /files/default-image.jpg
imgwidth: 480
imgheight: 300
categories: [legacy-blog]
tags: [news]
---
> This article is totally OUT OF TOPIC!!, however, it is still allowed to write this kind of articles according to this -web page-.

There are a lot of question when switching from your current position to a new project, specially refering to the success of a project, there are a lot of people who think the success of a new project is a random event, maybe it can work or maybe not, and there is no way to know exactly if something is going to fail, although that statement is somewhat true, checking the motivations is a good way to know if you are going to stand the problems, depending on the motivation, you can adapt to unexpected situation or complain and give up.

Basically, The motivation is the reason that you have to do something, if you ever ask WHY are you taking any action, then you will get the motivation, the motivations may influence in the expectatives greatly, affecting all the things to be done (and your behavior), we would say the motivations means; DOING something in order to achieve an EXPECTATIVE because of a GOOD REASON, for example, imagine a woman saying "I will DO EXERCISES in order to LOSE WEIGHT because ALL MY FRIENDS HAVE A GOOD SHAPE", although the expectative and the motives are different things, this is the way it works, every action that we take is because we have desired something and we have a good reason to do it, There are some common things it may expect to happen that are a result of not having the right motivation;

Unability to plan properly: bad motivations, usually, generate great (and wrong) expectatives, people who do not have the right motivation are more likely to set unreal deadlines or goals due to excesive expectatives that are really hard to achieve, those things usually happen because of desperation, people start fooling themselves thinking that they can achieve great goals in unreal periods of time, of course, this only leads to work hard on something that is never going to happen.

Not standing difficulties: people who do not have the proper motivation can not stand difficulties, this is because the most of them do not a real (or strong) reason to keep working in harder situations, the bad motivations give people a fake reason to do things, creating fake expectatives on them, when they start having difficulties during the project, then they realize that they do not have a good reason enough to keep working on the given project.

Giving up: This is just an outcome of the things mentioned above, having a reason and thinking that you can really accomplish the goals are the only things that will keep anyone working, if someone who has not both things will fail sooner or later.
The motivation plays an important role when starting a new project, it is important to know if the proper motivation, because if not there is a great chance that if the project starts to be difficult to handle, then you are more likely to desperate and fail, not because you could not do it, it is because you could not control yourself and the project, there might be some bad motivations that leads you to start a new project that will fail (unfortunately sooner or later it will fail).

### Anger or hate

The hate is a powerfull feeling, however, the most of its result are destructive not constructive, starting a new project because you hate your boss or someone else is not the right motivation, there are some things that will happen during the development of that project; first you will start to compare your results with that person, group or institution you hate, therefore, you will not be focused on the result that you want but only try to demostrate somehow that you are better than that person, second you will be productive (in theory) for a short period of time, as you may know, all the projects require at least a medium or long time frame to be completed, an explosive (and fleeting) productive frenzy will not help you.

### Money

Actually, this is not a bad motivation unless it is used along with another (good) motivation, if the fisrt, second and third motivation to start a project is money, money and money, then your blinded reasoning and your biased point of view will not allow you to set reasonable time frames and expectatives, this is because people which their only motivation is money (the bad way) are more likely to suffer of over-optimism, this will make people have a great optimism and expectatives about the goals without reasonable plans and backups, possibly, it may work, but when it comes to handling frustrations and disappointment the most of that people will fail and give up because they were not expecting anything of this. 

## External motivations

The peer pressure usully motivates people on doing things that they are not sure (or they do not want) to do, when you are doing things just because everyone else have done something it is a big mistake, as hate, you will start comparing with others, you will not be focused on what you really want or even if you really care about it, the worst thing about this is that even if you achieve your goals and their expectatives that would have no meaning for you.

the motivation must be based and focused on you, in other words, if your motivation is external then that is not a real motivation.

### dreams or goals
every action has a motivation, so if you feel that you do not know clearly what is 
