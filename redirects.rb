#!/usr/bin/env ruby

redirect_dir = "#{Dir.getwd}/redirects/"
file_path = "#{Dir.getwd}/_data/redirects.csv"
if File.exist?(file_path)
  file = IO.readlines(file_path)
  file.each do |line|
    redargs = line.split(',')
    redirect = {:from => redargs.first \
                , :to => redargs.last.gsub("\n",'') \
                , :from_ss => redargs.first.gsub('/','') \
                , :to_ss => redargs.last.gsub('/','').gsub("\n",'')}
    redirect_path = "#{redirect_dir}#{redirect[:from_ss]}_#{redirect[:to_ss]}\.md"
    redirect[:from] = "#{redirect[:from]}/" if !redirect[:from].match(/\/$/)
    
    if !File.exist?(redirect_path)
      new_redirect = File.new(redirect_path, "w+")
      new_redirect.write <<-REDIRECT
---
layout: redirect
sitemap: false
permalink: #{redirect[:from]}
redirect_to:  #{redirect[:to]}
---
REDIRECT
      new_redirect.close()
    end
  end
end
