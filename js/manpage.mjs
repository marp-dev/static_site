let totalHeight = 0,
    lineHeight = 30, //approximate line height in the document, this is arbitrary
    totalLines = 0

const body = document.querySelector("body"), //element that scrolls
      bar = document.querySelector(".bar"), //text that will change when scrolling
      title = document.head.querySelector("[name~=title][content]").content

function calculateHeight(){

	for( let el of body.children ){ totalHeight += el.offsetHeight }
	totalLines = Math.ceil( totalHeight/lineHeight )

}

function printLineIndex(){
	let lineNumber = (body.scrollTop == 0)?
			1 : Math.ceil( ( body.scrollTop * totalLines ) / totalHeight )

	bar.innerHTML = `Manual page ${title}(1) line ${lineNumber}/${totalLines} (press h for help or q to quit)`
}

export function init(){

	calculateHeight()
	body.addEventListener('scroll', printLineIndex);
	printLineIndex()

}

window.addEventListener('load', init)
