---
slug: how-generate-xml-sitemap-automatically-drupal
title: How to generate the XML Sitemap automatically on Drupal
summary: Drupal is a great CMS, however, it doesn't generate XML Sitemaps by itself, we'll see how to use the 'XML Sitemap' module.
date: 2013-01-15
imgpath: /files/sitemap.jpg
imgdescription: "[Giống sitemap](http://www.flickr.com/photos/inpivic/5205918163/) - [INPIVIC](http://www.flickr.com/photos/inpivic/)."
imgwidth: 480
imgheight: 300
tags: [web-design, website-structure]
---
Although Drupal is a great CMS, it has not a way to generate the XML Sitemap by default. However, there is a module that makes everything so easy that you would think I am cheating you. [The XML Sitemap module](http://drupal.org/project/xmlsitemap) generates automatically the XML Sitemap of all the content depending on the options previously configured by the user. It can even send the XML Sitemap to the Search Engines automatically. In this tutorial, we are going to see how to use this module to generate the XML Sitemap on Drupal.

## The Installation

The first step is to install the module. The installation must be done as any other installation. Either you can use Drush or Install it through Drupal's module administration page. In order to install it using Drush you just have to type the following commands in the shell on the Drupal installation path:

    drush dl xmlsitemap

by typing that command you would be installing the XML Sitemap module. You can also try to install the module through the Drupal's module administration page. In order to do it, you must go to the module administration page and click the "install new module" link. Then you can either download the module and upload it to Drupal or provide the FTP path to let Drupal download the module for you (this is the FTP path to install XML sitemap on Drupal 7: [http://ftp.drupal.org/files/projects/xmlsitemap-7.x-2.0-rc2.tar.gz](http://ftp.drupal.org/files/projects/xmlsitemap-7.x-2.0-rc2.tar.gz). The FTP paths are the download links in the [module page](http://drupal.org/project/xmlsitemap)). Click the "Install" button and you will have installed the XML Sitemap module.

## Enabling the proper sub-modules

The XML Sitemap module is based on multiple sub-modules that work together. In order to make it work. We have to enable the XML sitemap module along with the other modules. We have to select which of those modules should be active and which not. Here is a possible list with all the sub-modules:

- XML Sitemap; the main module, make sure to add the other modules because if not you will only see a sitemap with the link of the home page, just that.
- XML Sitemap custom; this sub-module can let you add custom links to the sitemap.
- XML Sitemap engines; this sub-module will upload the sitemap to the search engines automatically.
- XML Sitemap internationalization; this sub-module enable the sitemaps on multi-lingual Drupal installations.
- XML Sitemap menu; It enables the menu links to be in the sitemap.
- XML Sitemap node; It enables the nodes (the content, content types and everything related) to be in the sitemap, do not forget to enable this sub-module, it is really important.
- XML Sitemap taxonomy; this sub-module can add the taxonomy term links to the sitemap.
- XML Sitemap user; it adds the user profile to the sitemap.

As you can see in the list above. Although the XML Sitemap module is just one there are many sub-modules with different task. However, there are key sub-modules that you should enable. For example, if you do not enable the XML Sitemap node sub-module. Then the content of the whole sitemap is not going to be shown (it has no sense). If you have a multi-lingual Drupal installation and you do not activate the XML Sitemap internationalization sub-module then you are more likely to have only one sitemap with every link (with all the languages). It is really important that, at least, you activate the XML Sitemap node sub-module.

{% include image src="/files/XML%20Sitemaps%20sub-modules.jpeg" alt="XML Sitemap sub-modules" description="The functionality of the XML Sitemap module is based on multiple sub-modules." %}

## How does this module works

Depending on the sub-modules that you have enabled. Every group of content and all its children are going to have a new option identified as "XML Sitemap". Normally, if a group of content is enabled all its children will be enabled as well. For example, the XML Sitemap menu sub-module adds the options to the menus and each menu link. If a menu is enabled, all its menu links will be enabled by default. The XML Sitemap taxonomy sub-module adds the options to the taxonomy term lists and each taxonomy term. If a taxonomy term list is enabled then all its terms will be enabled by default. The XML Sitemap node sub-module adds the options to the content types and each node. If a content type is enabled then all its nodes will be enabled by default. There are two options, the first one is the "Inclusion" option. This options lets the content be included in the sitemap. It has a default value set to "Excluded". Once the content is included, the "priority" option may appear. It has a default value of "0.5". However, this value (as all the values) can be changed.

### _Adding the content to the sitemaps_

> Make sure to enable the XML Sitemap node sub-module in order to add the content to the sitemaps.

If you have the required modules available you can add the content to the sitemaps (the nodes more specifically). In order to do it, you should set the options at the content types. You can enable the content type to be written in the sitemaps and set the priority for all that kind of content.

{% include image src="/files/XML%20Sitemap%20-%20content%20type.jpeg" alt="XML Sitemap - Content type" description="The content types are going to have a new option section, these option will be inherited by all its associated nodes." %}

If you enable the content type, all the related content will be written in the sitemaps. However, you can still change the configuration of each node, so if you want every node to be sent but one node you can enable the content type and disable the node.

{% include image src="/files/XML%20Sitemap%20-%20article.jpeg" alt="XML Sitemap - Article" description="This is the new option section in every node, the nodes are excluded by default, you can either include its content type or just the node itself." %}

### Adding the menus (navigation) to the sitemaps

> Make sure to enable the XML Sitemap menu sub-module in order to add the content to the sitemaps.

You can also add the menu links to the sitemaps. In order to do that, just do the same as described before. Every menu (and all its links) in the Menu administration page of Drupal will have a new option. You can either include the whole menu in the sitemaps.

{% include image src="/files/XML%20Sitemap%20-%20Main%20menu.jpeg" alt="XML Sitemap - Main menu" description="The menus can be added to the sitemap." %}

Or include just a single link.

{% include image src="/files/XML%20Sitemap%20-%20menu%20link.jpeg" alt="XML Sitemap - Menu link" description="You can also add just a single link to the sitemaps." %}

If you add the whole menu all its links are going to be included too. You can change the options for each link if desired.

### _Adding the taxonomy term lists to the sitemaps_

> Make sure to enable the XML Sitemap taxonomy sub-module in order to add the content to the sitemaps.

The taxonomy term lists can also be included to the sitemaps. The procedure is just the same. You can include either the whole taxonomy term list.

{% include image src="/files/XML%20Sitemap%20-%20taxonomy%20term%20list.jpeg" alt="XML Sitemap - Taxonomy term list" description="Even the taxonomy term lists can be added to the sitemaps."%}

or a single taxonomy term.

{% include image src="/files/XML%20Sitemap%20-%20taxonomy%20term.jpeg" alt="XML Sitemap - Taxonomy term"%}

If you include the whole taxonomy term list then all its taxonomy terms will be included by default. If not you have to include every single taxonomy term manually.

## The Configurations

The configuration options of the XML Sitemap module (and all its modules) are located in the configuration options of Drupal. Here you can add custom links (with the proper module previously enabled). Customize the upload process to the search engines. Rebuild the links of the sitemaps and perform any other configuration activity related to this module.

{% include image src="/files/XML%20Sitemap%20-%20Configuration.jpeg" alt="XML Sitemap - Configuration" %}

You can add a custom link to the sitemaps in the "Custom Links" tap. In order to add a custom link the supplied path must be accessible. Additionally, it must be based on the URL of the website. This is for situation that an external webpage must be added within Drupal. For example, if I want to add a  webpage of a free software project in my website that I could not built in Drupal. I can still add the webpage by giving access to a specific path in my domain and adding that link to the sitemaps through this configuration panel.

{% include image src="/files/XML%20Sitemap%20-%20Custom%20links.jpeg" alt="XML Sitemap - Custom links" description="You can add custom links of external webpages in your domain to the sitemaps."%}

The XML Sitemap module has the capability to upload the sitemaps to the search engines. You can customize how often the sitemaps should be uploaded and which search engines should receive the sitemap. However, there are some details to be warned of. First you are more likely to need the "[Site verification](http://drupal.org/project/site_verify)" module in order to verify the ownership with the search engines. Additionally, you must be sure that the Drupal cron runs regularly.

{% include image src="/files/XML%20Sitemap%20-%20Search%20engine%20submission.jpeg" alt="XML Sitemap - Search Engine Submission" description="You can customize how often the sitemaps are submitted to the search engines."%}
