---
slug: what-will-you-find-my-blog
title: What will you find in my Blog
summary: hello guys, welcome to my site, as you may know, I am just starting with this, there is a couple of things that I'd like you to know about my blog.
date: 2012-10-08
imgpath: /files/newspaper.jpg
imgdescription: "[Newspaper B&amp;W \\(5\\)](http://www.flickr.com/photos/62693815@N03/6277209256/) - [NS Newsflash](http://www.flickr.com/photos/62693815@N03/)"
imgwidth: 480
imgheight: 360
tags: [news]
---
Hello everybody! welcome to my website. This is a brief explanation about the blog of this website and what you will find in it. This is a blog of web technologies dedicated for developers, therefore I will suppose that you already know some basics about programming and software development. The kind of topics I would cover would be **any topic that is related with web technologies such as; drupal, jquery, PHP, HTML5, and so on**. The next year I will try to be focused on these four technologies mentioned before (drupal, jquery, PHP, HTML5). **There will be no preference about server-side or client-side**.
Although a topic must be related with web technologies in order to be posted. **There will be some other articles I would like to cover that are related to productivity**. This is somewhat out of topic, but it is something I would like to do. Besides, **those articles will be posted seldom compared to the other articles**. There are several types of articles I am going to post in the blog grouped in three categories; discussions, tutorials, productivity.

**Discussions**: These kind of articles are focused on the "what" & "why" of a topic. **You might find theorical discussions about tools, technologies, activities or methodologies**. These articles lack of technical guide and step2step solutions. **I will try to post at least 1 of these articles monthly**.

- Reviews; Discussions about a specific product or service, defining it, explaining its features and pros & cons, and evaluating it based on a personal previous experience. 
- List based article: Explanation of a list of reasons or facts with a specific topic. The list would be numbered or not.
- Comparisons; This article is self explanatory. A comparison between products or services, explaning the features and pros & cons of each one.
- Informative; It is pretty similar to a how-to article but theorical. It is an explanation a specific fact, describing why something is (or is not) happening and how to solve it.
- Opinion; Self explanatory. A thorough description of a personal opinion about something.


**Tutorials**: These kind of articles are focused on the "how" of a topic. **These articles will explain, detail or guide how to make/solve something with technical guides or step2step solutions**. These articles lack of theorical information. Every article will make a brief description about the skills, kownledge and tools needed to understand the article properly. **I will try to post at least 1 of these articles monthly**.

- how to ...; This is so self explanatory that I do not even have to mention it. It is a thorough step2step description to make/solve something.

**Productivity**; These article will try to **explain some productivity issues that are holding you to get better results** and how can you get rid of those issues. **I will try to post at least 1 of these articles each three months**.

- Inspirational; This type of article will encourage you to take decition that should took a long time ago.
- Informative.
- Opinion.
- List based article.

Additionally, **there are news in the blog, but referring exclusively to news of this website**.
