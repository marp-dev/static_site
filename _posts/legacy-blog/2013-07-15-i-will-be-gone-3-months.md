---
slug: i-will-be-gone-3-months
title: I will be gone for 3 months
summary: I have something really important to say; I am gone for three months. I hope to be back soon.
date: 2013-07-15
imgpath: /files/newspaper.jpg
imgdescription: "[Newspaper B&amp;W \\(5\\)](http://www.flickr.com/photos/62693815@N03/6277209256/) - [NS Newsflash](http://www.flickr.com/photos/62693815@N03/)"
imgwidth: 480
imgheight: 300
tags: [news]
---
Hey people, how is it going?. I have been enjoying a lot this role as a blogger writing for all of you. I have received many good reviews encouraging me to keep it up as well as many suggestion encouraging me to improve some things. Although I have intentions to keep blogging, I must say I really need to back up for at least 3 months to improve the website, improve the existing content and write new content. I will explain why:

- I feel I must improve the existing content because it has several writing mistakes that I really need to fix, additionally some articles are too wordy and some important statements are almost unseen making them somewhat unclear.
- I was supposed to have a release schedule in order to publish articles periodically, but I have totally broken it. Unfortunately I am currently publishing an article monthly due many reasons (long writing process, long translation process, several reviews, laziness) and I really need to do something about it.
- I want to make a new version of the website. That includes many technical improvements going through the server itself to the html and the content. I am currently developing a new theme, and I have many other ideas to do.

I have planned to come back with everything on November 1 of 2013. Until then I will not publish any new content. I hope that everything goes well and can get back to blog as always. I hope that you understand. Thanks.
