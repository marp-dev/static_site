---
slug: top-3-neglectful-and-irresponsible-programming-mistakes
title: Top 3 neglectful and irresponsible programming mistakes
summary: Are you capable of anything to complete a work that has taken more than expected? check these 3 irresponsible programming mistakes.
date: 2013-06-24
imgpath: /files/Carelessness.optimized.jpg
imgwidth: 480
imgheight: 300
imgdescription: By US [Treasury](https://home.treasury.gov/), via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Carelessness_causes_accidents..._Accidents_slow_up_production._-_NARA_-_535274.jpg)
tags: [tips-and-tricks, productivity, programming]
---
> EDIT: Please interpret this article as a sort of autocritic... 

Have you ever felt desperate to finish a task or accomplish a goal or objective?. Have you ever thought about deliver any result even if is not what the client/boss was expecting?. **Are you capable of forgetting about rules, principles and ethics in order to finish something that has taken more than expected?.** Although everyone has felt tired or stubborn at least once in his life, there are some things that can not be never skipped (unless you want to have really big problems). This represent one of the 3 neglectful and irresponsible programming mistakes that we are going to review.

## Doing ANYTHING to complete the work

These mistakes are all those committed by people who are capable of anything to complete the work. When I say "anything" I mean doing anything that is necessary even if it affects you. These are some of the things that implies doing anything to complete the work.

#### <u>Delivering partial results</u>

This is the most common of the reasons to say why someone is doing anything to complete the work. **It basically consist on delivering something similar to the result and not what it was expected. This may also come along with excuses to relieve you of the responsibility to do the right thing.** Most of these excuses will state "that's not my problem" or "it's someone else problem". The only reason to deliver other results is when all the parties knew about a possible condition, problem or situation that could arise even before starting the project.
It is really important to understand the objectives very well before starting a project. Some inconsistencies which represent a lack of scope may end up in a situation that will force you to work more hours than expected even if the objectives are practically completed. 

#### <u>Disabling features such as security or data consistency measures</u>

If you are capable of commenting validations in the code, turning off security features in the server (instead of configuring them) and removing SQL transaction sentences advisedly, then you are showing not just a total desperation but a complete unattachment to the rules. This kind of actions may take great consequences not only for you but for the whole team as well. **Most of cases you might be creating a security hole compromising the system, its data and the whole business.**
In my opinion, this kind of things are only accepted by irresponsible people as well. **It is pretty important to check this kind of things before updating the production environment.** Unfortunately these kind of security holes are responsibility of the whole team not in one of its members only.

#### <u>Messing with the core</u>

**In cases when you have to deliver something built on a tool/framework/API/system and the system you are using is not acting properly; the problem is not the system itself, your work is.** It is totally irresponsible to mess with the core of the system or any of its modules in order to achieve the goal, especially in these cases. Have you ever imagined what can happen if the system is updated? have you ever imagined what will be the behavior of the system from now on?. When performing this kind of changes you can not guarantee that the system will work properly. **If you really think the system has a bug, make sure to gather all the proves necessary but do not perform this king of changes.** Try to find an alternative (and right) solution instead. If you ever do something like this you are demonstrating that you do not have respect for ethics (and your client).

## Not respecting the structure, rules and principles

There are other mistakes that can be committed in cases when you have to deliver a software that is a part of an existing program. Most of these mistakes has to do with the changes performed badly to the system. This does not have to be a big problem unless those changes are allowed something that is totally irresponsible as well.

#### <u>Performing changes without considering the current structure of the system</u>

Usually, prior to every building process of any software its design is always created. Generally this design is created in a way that the software can be easily maintained. If you ever are going to perform any change to a software, never violate its design not matter how little can be the change because you might compromise these things. **Terms as "Maintainability" and "Extensibility" refer directly to time and has to do a lot with money. It is demonstrated that programs which are hard to maintain are usually more expensive due to the high costs they generate.** This usually also happens to programs which are hard to extend because, in those situations, performing great changes always take time and sometimes those changes are discouraged. Therefore applying changes that can compromise the maintainability, extensibility or any other feature carefully thought in the design of the software is totally irresponsible because you would be generating more costs over time.

#### <u>Performing changes without considering rules and principles</u>

Throughout the whole computer science career at universities students are warned about several principles, especially at the beginning when the first courses are taken. Most of these principles are based on simple recommendations such as avoiding data redundancy and thinking before acting, most of them are based on facts proven over time. Perhaps forgetting about these principles due to a lack of attention may be a mistake that can be forgiven (and work harder on it), but **doing this deliberately and advisedly demonstrate a neglectful and irresponsible behavior and a total lack of interest in good results.**
Principles are not the only the only rules to follow. Depending on the project, there might be some other rules to follow. These rules can cover a lot of subjects like coding standards, source control, testing and delivered results. Although in most of these projects the members are always forced to abide these rules, even thinking about breaking them shows a total lack of respect and attempting to do it is totally irresponsible (in this case is irresponsible for both parties; for the one who attempted it and the one who allowed it).

## Pretending you know more than the user

These mistakes are really special. In my personal opinion is the perfect example of negligence. These mistakes usually happen when someone think he knows more than the user. When that happens that person literally does not care about the security of the system anymore.

#### <u>Believing that the user will need expert skills to surpass the security</u>

One of the reasons why someone may think really strong security measures are too much is that (apparently) most of users do not have skills enough to break them. This mostly happens when it comes to web development. People who think like this usually commit several mistakes generating security gaps in the system. one of the most emblematic cases is VTiger 2.5.1. This version of VTiger has a bug in one of its validations because it is only performed in the client side. The validations are performed using JavaScript. Depending on the browser, if you hit several times "Enter" you will break the browser and the system will allow empty inputs on mandatory fields because the validations are not performed in the backend. These kind of bugs are the result of thinking the user will never find out how to break the security. They will break the security stupidly and show you how neglect you are.
By the time you pretend the user knows as much as you, you will start taking responsible decisions about security. **The ignorance of the people does not depend on you, it depends on the people. Therefore if the security of the system depends on the ignorance of the people, then the security will depend on the people not on you.**

#### <u>Believing that the user can not do anything to the server</u>

Another reason why some people think really strong security measures are too much is that the user will only use the client to communicate with the server. Some people believe they can prevent every single user action because the user is forced to use the client and they do not have a direct control over the server. This is also usual when it comes to web development. Sometimes people who think like this may ignore that webservers, frameworks and programs do have bugs as well and let accessible programs such as phpmyadmin (just to mention an example). **Due to the great amount of exploits out there for many existing programs thinking like this is totally irresponsible,** even unexperienced users can access this information freely. It is proven that several sites are hacked for this reason, therefore taking as many security measures as possible is expected nowadays.

## EXTRA

Additionally to these mistakes, there are some other irresponsible acts usually committed prior to accepting a contract/project. Most of them are based on the fact that you are accepting all the terms of the contract even if you have not read them. That means that even not reading very well is totally irresponsible.

#### <u>Keep in mind when you accept the terms of a contract you are giving your word</u>

If you ever take a contract without understanding very well the terms of a contract, its rules, objectives and tasks, you are giving your word on something you are not sure about. This will become instantly in a negligence that might destroy your reputation. Depending on the results it will become in an obstacle for you to progress in your professional career, preventing you to take more opportunities. Additionally, I would like to state that the life itself will give you what you are giving to your neighbor. If that is poor results then you will have poor benefits as well.

#### <u>Are you really capable to finish the work?</u>

Be honest with yourself, are you capable to finish the job? or are you thinking only about the money?. Do you have the required tools and skills?, are you sure you are not missing anything?. I know it is pretty good to be brave enough to do certain things, but that does not include to do things you do not know. The uncertainty and ignorance are one of the hardest things to fight simply because you will never know exactly when you already have a solid knowledge base, so please do not be irresponsible and do not measure the uncertainty. 

#### <u>Is the timeframe available fair enough to complete the work</u>

Whether if someone is assigning the timeframe to you or you are negotiating the time you must make sure it is fair enough because if not, once you accept the terms, it would be a negligence from you. Every single step of the processes to complete the objectives must be consider.
