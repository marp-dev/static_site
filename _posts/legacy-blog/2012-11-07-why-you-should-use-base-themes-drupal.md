---
slug: why-you-should-use-base-themes-drupal
title: Why you should use base themes on Drupal
summary: Drupal is not easy to theme, but that doesn't mean that you have to waste your time each time you do it, right?
date: 2012-11-07
imgpath: /files/website-mockups.jpg
imgdescription: "[mocking up manually](http://www.flickr.com/photos/girlgeek/3272284165/) -&nbsp;[jez\\`](http://www.flickr.com/photos/girlgeek/)"
imgwidth: 480
imgheight: 333
tags: [drupal-theming, theming, base-themes, web-design]
---
> Despite of I am giving advices of theming and design. I am not a designer. I am a developer and the best I can do is to make something look good yet functional.

Starting the construction of this website on Drupal. After the analysis of the design I was going to make. I had to face the theming process. The process that I was supposed to implement the thought design in Drupal. There were some requirements for this theme such as its responsive layout and its HTML5 and CSS3 bases. I had to figure out how to do it by myself. I made 3 versions of this theme, one was made from scratch, one was based on Adaptive Theme, and the last one was based on Omega. Analyzing this experience, there is a big difference of theming from scratch and theming based on another theme. You can, not just, avoid this total waste of time you can also make a theme with better features and easy to maintain and modify.

## Responsive layout

As one of the requirements for this theme. It was necessary to implement a fluid grid solution such as Columnal or 960.gs into the theme, and possibly, with the rest of the modules that could generate an output.

### Theming from scratch

- **You have to integrate a fluid grid into the theme manually;** you have to integrate the fluid grid (Columnal or 960.gs) into the theme manually, maybe it is not the big deal. You just have to set some CSS classes and HTML code, but it would get worst if you have to add CSS code to an HTML code that it is in a variable. If it is that so, you will have to use a theme function just to find the variable and add the proper modifications in order to get the grid working with your theme.
- **When integrating a fluid grid, you have to set all the CSS classes manually and it would get worst if you have to integrate it to other modules (e.g.: panels);** This is somewhat related with the last point. You might want to add some CSS classes to get the grid working with a module like panels. Adding a CSS class to a panel maybe is not that difficult, but, if the grid requires a specific structure to work and not just some CSS classes, it will be difficult to implement it. 


### Using base themes

- **The responsiveness of the most of base themes is self managed;** Base themes such as omega and Adaptive Themes have separated CSS files size with guidelines of how to craft themes using these files. All of them have a “mobile first” design, making sure that the theme will work on every device.
- **User-assigned width;** Base themes as Omega have the capability to let the user assign the width of the sections keeping the same responsiveness. This change entirely the theming process into something pretty simple. Unlike base themes, the sections of the themes made from scratch have to have a preassigned width.


## Integration with modules

Each module in Drupal has its own output with its own HTML and CSS. If one of those modules does not print the output as it should or prints an undesired output you will have to modify or fix that output by any means necessary. Of course, this is considered as minor change, but those minor changes have to be solved anyway and they can take some time to be solved.

### Theming from scratch

- **You are supposed to change every single UI characteristic of every single module to match it with the theme;** There are some modules that its output may not be the output that its being desired. Maybe the comments, the titles or the menus has to be changed because it does not look as it should.
- **You have to fix every single UI characteristic that could break a visual style of your theme;** There are some other modules that may cause the theme to break its look. Some modules have their own margins, paddings and borders, each of these properties that may generate conflicts with the theme have to be fixed.

### Using base themes

- **Customizations and fixes for mayor modules;** Unlike themes made from scratch, the base themes have their own customizations. Some of them can provide minor fixes for those modules that are generating a conflict with the theme. Additionally, base themes as omega have the capability to let the user toggle any CSS file to deactivate any undesired style.

## Theme zones and sections

Drupal blocks can let the user arrange the content in the user interface. There are also other modules that gives more capabilities to arrange the user interface as the user likes, but, some sections and zones are arranged in the theme by code. Letting the user move these section would be challenging if the base themes do not exists. The fact is that the sections of a website is not something that it should be changed. It was supposed to be planned, thought and agreed upon, but making sure that the sections and zones can be arranged by someone else than the designer/developer (or any other responsible for the theme) will improve the maintainability of the theme.

### Theming from scratch

- **You have to set all the zones and sections by coding it;** As I mentioned before. Drupal blocks let the user arrange the content but can not let the user change anything more than that, e.g.: the user can not change the sidebar and the content section (their position, width or visibility). The theme can have a fallback for a section that has no content. Maybe, it is possible to remove the content for a section on a specific page in order to disappear it. There are other modules as panels that can override the default sections of the theme to print the content as the user needs, but this is not giving real power to the user. It is more like an alternative for something it can not be possible with a theme made from scratch.


### Using base themes

- **Sections arrangement;** Base themes as Omega let you modify or hide every single section and zone. They let the user change options like position, size and visibility.

## Other benefits of theming with a based theme

- **HTML5 ready;** The mayor base themes for Drupal give a great support for HTML5 by default. They provide libraries, polyfills, optimized code and configurations helping to focus just in the theming process.
- **Predefined style;** The mayor base themes have their own predefined styles. They have their own buttons, tables, blocks and menus. They look really good and they can (possibly) match your design.
