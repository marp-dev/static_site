---
slug: things-you-should-do-support-css3
title: Things you should do to support CSS3
summary: Why does IE make everything so complicated?, these are the things you should do to support CSS3 on older browsers.
date: 2012-11-18
imgpath: /files/ie.jpg
imgdescription: "[Internet Explorer voo doo](http://www.flickr.com/photos/volantra/3406410663/) -&nbsp;[volantra](http://www.volantra.co.uk/)"
imgwidth: 480
imgheight: 300
tags: [css, css3, html5]
---
> EDIT: no one should have a reason to use IE.

Technology has changed the life of everyone, especially internet. Lately, there are available a new generation of internet browsers with new features that let create rich internet apps and awesome websites. They provide tools for connectivity, presentation, performance and much more. However, there is a problem. People who do not update their browsers. This is making the use of CSS3 challenging, but does this mean that using CSS3 is discouraged?. Of course not, there are a couple of things that you can do in order to use it.

## Before start, check it out in caniuse.com

Before saying anything it is important that you know exactly what you are going to do. I mean with "exactly" that you must know previously every style, shape, layout and animation that you will use in your design. Because you must check every single feature that you are going to use. This is really important. You can not skip this step. **If you rely too much on a feature that is available just for a few browsers you will have a big problem**, because there will be no way that you can support those other browsers.
I strongly encourage to check every single feature that you are going to use on [caniuse.com](https://caniuse.com). This is a website with a thorough description of every single CSS3 feature with its respective table support. There are also websites like [html5please.com](https://html5please.com) that shows a description for a lot of CSS3 features, occasionally with their advices.

## Use Modernizr

One way to support browsers that can not handle some CSS3 features is to set a fallback. Simply if the given feature is not available you will use another (similar) feature instead. It is not necessary to use an special library to set a fallback in the most of the cases, but if you want to give a better support it will be necessary that you use [Modernizr](https://modernizr.com).
Modernizr is a JavaScript library that will check every CSS3 feature available. It provides JavaScript methods and CSS classes to give a way to apply actions depending on the features available. It is based on YepNope and  it is pretty handy. I would say, necessary. Actually, If you are willing to use CSS3 then you will have to use Modernizr. It loads at the beginning and it will not interfere with any other thing, even the performance. When you use Modernizr the HTML element `<html>` will have a couple of CSS classes with the supported and unsupported features. Letting you to define the CSS3 code per case, like this:

    .cssanimations.csstransforms3d #element{
    /*code here*/
    }

    .no-animations.csstransitions.csstransforms #element{
    /*code here*/
    }

For example, this code will check if CSS3 animations and 3D transforms are available. If not it will check if CSS3 transitions and 2D transforms are available. Maybe it can arise situations that you must set this kind of validations. If something is not available you will use something that looks pretty much the same. As you can see, **using Modernizr is pretty simple, it will ease you of doing all that detection all for yourself**.

## Use polyfills

A polyfill is an alternative solution for a feature in order to support older browsers. There are a lot of polyfills available for any purpose. There are plenty of them for IE (of course, this is the browser that gives more problems) and the most of them work with HTML Components (only works on IE). They will provide you an HTC file that you must load in a CSS property called "behavior", something like this:

> EDIT: This 'behavior' is only for certain versions of IE and has a lot to do with CSS3PIE (there is more about it in the section bellow). Additionally, a polyfill can be actually made in plain javascript for other kind of browsers like Opera that do not support certain features. I guess I fogot to mention this by that time -__-". Please remember this is a very old blog post. Take everything with a grain of salt.

    #element{
    /*CSS3 unsupported code here*/
    behavior:url(/file.htc);
    }

This file must be used at the bottom of each CSS block using unsupported features (you may like to use Modernizr here). Once loaded, the CSS3 code will work as usual. These HTC files must be accessible each time it is being used. Making the file available directly through the server is the best option. You can set an Alias in the server if you are working with Apache.

    Alias "/file.htc" "/path2file/file.htc"

You must do this for every polyfill that require to load a HTC file. In some cases **there might exist extensions, modules, or similar, offering a way to load these HTC files without doing this (I have seen these modules on Drupal before). Installing these modules is highly discouraged. They will only add code for nothing**. We are going to see some polyfills that might help to support CSS3 on IE.

### CSS3PIE

This is one of the most useful polyfills I have ever seen. It support a lot of CSS3 features for older browsers (IE). It supports box-shadow, border-image, border-radius and rgba colors. Features that are widely used in internet. The way to install it and use it is the same as above. They will give a HTC file. You will load it in your CSS code, and work as usual.

### Box-sizing polyfill

If you are going to use border-image then I strongly suggest to use [Box-sizing polyfill](https://github.com/Schepp/box-sizing-polyfill). This polyfill will support box-sizing for older browser. There are situations when you use a border (either thin or thick) the container of the information may measure more than it is expected. That could look bad or even can break the layout (e.g.: if there are floating DIVs with a percentile fix width next to each other, just as if it was a row, and one of those has a border it can make one of the other containers fall down to the other row). This polyfill provides an HTC file that you must use in you CSS code (as I mention before).

## Be aware of CSS3 Features harder to support

Unfortunately, there are some features that are really hard to support for older browser, some others have a questionable performance (I am not saying bad, just questionable) and you should be aware of this before trying to use one of these features.

### Border-image with 'repeat' scheme
Sadly, there are no polyfills that can support this feature. They only support border-image with 'stretch' scheme. I was a victim of this too, but if you ever need to support this feature for older browser. You have replace the borders with DIVs and restructure the HTML in a way that the DIVs look like the border you are searching for.

{% include image src="/files/styles/original/public/warn-style.jpg" alt="Warn style" description="Warn style" %}

This picture is showing one of the styles it was defined in the CSS code of this website. this style is simply not supported for Internet Explorer. There are neither no polyfills available to solve this problem nor fallback it can be applied. In this case, the only solution I could found for this problem is not using this style and restructuring the HTML in the sections that I had to apply this style. For example, I had to apply this style to the highlighted section (the opening door section). The solution was to put two DIVs that look like the borders I wanted to put, like this;

{% include image src="/files/border-image-workaround.jpg" alt="Border-image with &#039;repeat&#039; scheme (Workaround, CSS)" description="Border-image with &#039;repeat&#039; scheme (Workaround, CSS)" %}

There might exists other cases where applying this style is impossible. Unless you do not want to support older browsers, this is something that you should be aware of before doing anything with CSS3.

### Box-shadow

There are polyfills supporting this feature, but it is not fully supported. The performance is (obviously) not the same to the browsers that support CSS3 natively. Additionally, there are a couple of things that no polyfill can do.

- Inner shadow: There are no polyfills supporting this feature (by now). Inner shadows can be very useful, especially if you change the color of the shadow. This is used to make good-looking buttons and many other things. There are things that you can do depending on the situation. You can set a fallback to the box-shadow or you can replace the entire shadow with something similar. Anyway, the solution is to replace the whole shadow. For example, each button of this template has been made with an light upper inner shadow and a dark lower inner shado. This makes the button looks like a button. If the browser does not support inner shadows it will replace the shadows with a light upper border (with a color similar to the button) and a dark lower border.

{% include image src="/files/styles/original/public/buttons.jpg" alt="Buttons (Shadows, CSS3)" description="Buttons (Shadows, CSS3)" %}

Using a border to replace those shadows is a pretty simple idea that works. It is still well-look button without having to do anything special, but there are other cases that the best solution is not using inner shadows.

{% include image src="/files/work-around.jpg" alt="Inner shadow workaround" description="Inner shadow workaround" %}

If you have to place an Internal shadow to a container the best solution is to put objects in the container so their shadows will make the container look as if it has internal shadow. There is no other way (or at least I could not found another alternative), although is not that simple. It does not look bad. We might say it looks good and we are solving the problem anyway, but it might get worst depending on what you are going to do. Be aware that supporting box-shadow is not simple. I encourage you to know exactly what you have to do and check if you can really do it.

- Unexpected behavior: The shadow may have not the same transparency than the native browsers (in fact, it is opaque). The shadow it is more like an object. Therefore, the shadow has not a natural look. Depending on the position of the element generating the shadow. The shadow may overlap another element making it unclickable. You should be aware that it might have not the same performance and stability as native browsers (I am blaming Internet Explorer, not the polyfills).

### CSS3 Animation/Transition performance issues
HTML5 and CSS3 are being developed and improved day by day. but the support and performance for each browser is not the same. That includes CSS3 animations and transitions. A well supported website is supposed to use each feature on each browser with a similar performance each other. There are some things that you should not do when animating with CSS3.

- Animating big elements: Animating small elements is not the big deal, but there might be some browsers that its animating performance could worsen when animating bigger elements. Perhaps dividing the animation into smaller pieces or animating smaller elements might be a better idea. There are some initiatives trying to implement better methods for animation such as RequestAnimationFrame, but those technologies are still immature. Another way to solve the performance issues is to use an alternative such as canvas or SVG. Although you could have the same problem if you are trying to animate bigger elements. The performance could get even worse if the animation involves the modification of multiple properties of that element being animated. The performance could get so bad that the result could be catastrophic. As I mention before, you should think about those things before doing anything.
- Animation overload: All the browsers are really different from each other, therefore they will handle the load differently. Some of them are capable to handle big loads with an overall good performance, but unfortunately that does not happen with all of them. **Large animations where everything is activated at the same time all the time unnecessarily can be so sluggish and clumsy that they are even capable to freeze some browsers**. Think as if the animation was a scene of a theater play. Every actor has a role and a time to perform. The animation should work the same way. Do not activate any element unnecessarily.

## Latest advices

Unfortunately, no matter how hard you try you can not support every browser. Almost all programs in the world work like this. They support a specific number of platforms and if a newer program is released. The newer and most used platforms will be supported. Every web application should work like that (even websites of course). Besides, I do not think that updating a browser or installing a new one it is so hard. **At present CSS3 is supported by a wide range of browsers of every platform. Therefore, if you really want to use a new CSS3 feature. Then use it in a way that all the browsers can display the website even if it does not look the same**. Of course this is just a personal thought.
