---
slug: buttons-and-menus-animation-only-css3
title: Buttons and menus with animation only with CSS3
summary: Are you still using JavaScript to make dropdown menus? See how to make dropdown menus with effects only with CSS3
date: 2012-11-28
imgpath: /files/Menu.jpg
imgwidth: 480
imgheight: 300
tags: [tutorial, css, css3, html5, animations]
---
> EDIT: WARNING. This blog post is very old. Although this tutorial might code might work. Many of the explanations might be outdated or do not have sense at all. Read everything with a grain of salt.

> Be aware that [Modernizr](https://modernizr.com/) is being used in this tutorial. Additionally, the behavior of the menu and its buttons may not be displayed the same way for every browser.

> What you'll learn:
> - Using CSS3 animations & transitions
> - Giving a good look to objects with shadows

> Requirements:
> - You should know some HTML & CSS basics 
> - use Modernizr

In this tutorial we are going to see how powerful is CSS3. We are going to create a menu up to three levels with effects only with CSS3 and nothing more. No JavaScript, no flash, nothing more but HTML and CSS. The tutorial is divided in two parts, the creation of the buttons where the look and other things strictly related to the buttons will be seen, and the creation of the menu where the structure and behavior of the menu will be considered. **The code bellow is the whole CSS code that we are going to analyze. The code of the menu will be considered in the 'creation of the menu' section.**

    /*begin buttons style*/
    ul.menu a{
      display:block;
      text-decoration:none;
    }
    input[type=submit], button, ul.menu a{
      width:8em;
      text-align:center;
      height:1.8em;
      line-height:1.8em;
      background-color:blue;
      font-size:1em;
      color:black;
    }
    .borderradius input[type=submit], .borderradius button, .borderradius ul.menu a{
      border-radius:3px !important;
    }
    .boxshadow input[type=submit], .boxshadow button, .boxshadow ul.menu a{
      box-shadow: -1px 1px 6px rgba(0,0,0,0.5), 2px -2px 2px 0px rgba(0,0,0,0.5) inset, -2px 2px 2px 0px rgba(255,255,255,0.5) inset;
    }
    .no-boxshadow input[type=submit], .no-boxshadow button, .no-boxshadow ul.menu a{
      border-top: solid 1px #8888FA;
      border-left: solid 1px #0000A1;
      border-right: solid 1px #8888FA;
      border-bottom: solid 1px #0000A1;
    }
    .boxshadow input[type=submit]:active, .boxshadow button:active, .boxshadow ul.menu a:active{
      line-height:2em;
      box-shadow: 0px 0px 0px rgba(0,0,0,0.6), -2px 2px 2px 0px rgba(0,0,0,0.5) inset, 2px -2px 2px 0px rgba(255,255,255,0.5) inset;
    }
    .no-boxshadow input[type=submit]:active, .no-boxshadow button:active, .no-boxshadow ul.menu a:active{
      border-top: solid 1px #0000A1;
      border-left: solid 1px #8888FA;
      border-right: solid 1px #0000A1;
      border-bottom: solid 1px #8888FA;
    }
    input[type=submit]:hover, button:hover, ul.menu a:hover{
      color:#C55353;
    }
    .boxshadow input[type=submit], .boxshadow button{
      border:0;
    }/*end buttons style*/
    /*begin button transition*/
    .csstransitions.boxshadow input[type=submit], .csstransitions.boxshadow button, .csstransitions.boxshadow ul.menu a{
      transition: color 0.5s, line-height 0.3s, box-shadow 0.3s;
    }/*end button transition*/
    /*begin menus*/
    ul.menu, ul.menu li{
      display:block;
      position:relative;
      padding:0 !important;
      margin-bottom:0 !important;
      margin-left:0 !important;
      list-style:none !important;
      list-style-type:none !important;
      list-style-image:none !important;
      list-style-position:outside !important;
      float:none;
    }/*end menus: all menus are defined here*/
    /*begin menu options*/
    ul.menu li{
      margin-right:2em;
      float:left;
    }
    ul.menu li.last{
      margin-right:0px;
    }/*end menu options*/
    /*begin all submenus*/
    ul.menu ul{
      padding:0;
      display:none;
    }/*end all submenus*/
    /*begin 2 level submenu*/
    ul.menu li:hover ul, ul.menu li ul li:hover ul{
      display:block;
      animation:fade 3s;
      animation-play-state:running;
    }
    ul.menu ul{
      position:absolute;
      top:1.8em;
    }
    ul.menu li ul li{
      clear:both;
    }/*end 2 level submenu: the submenu has an excessive width and height to prevent clipping*/
    /*begin 3 level horizontal submenu*/
      ul.menu li:hover ul ul{
      display:none;
    }
    ul.menu li ul li:hover ul{
      display:block;
    }
    ul.menu ul ul{
      position:absolute;
      left:100%;
      top:0;
    }/*end 3 level horizontal submenu*/
    /*begin main menu animation*/
    @keyframes fade
    {
    0%   {opacity:0;}
    100% {opacity:1;}
    }/*end main menu animation*/

## Creating the buttons

First, we have to set a style for all the buttons of each menu. Technically this buttons style is applied to the link elements in the menus. Additionally, we are going to set the same style to all the button elements and the buttons in all the forms. All the buttons will have a fixed width and height. **Keep in mind that the text for each button should not be too long**. All the buttons have a specific behavior in case the user hover or click the button. We can also set a transition so the properties will change smoothly. We are going to review the code step by step.

    /*begin buttons style*/
    ul.menu a{
      display:block;
      text-decoration:none;
    }
    input[type=submit], button, ul.menu a{
      width:8em;
      text-align:center;
      height:1.8em;
      line-height:1.8em;
      background-color:blue;
      font-size:1em;
      color:black;
    }
    .borderradius input[type=submit], .borderradius button, .borderradius ul.menu a{
      border-radius:3px !important;
    }

{% include image src="/files/styles/original/public/buttons_0.jpg" alt="CSS3 Buttons" description="The measures of the buttons are fixed." %}

This is the first part of the definition of the buttons. The button must have fixed measures, so the 'width' property of all the buttons was set to 8em and the 'height' and 'line-height' properties were set to 1.8em. The 'line-height' property was defined in order to animate the text inside the button. The 'height' property will prevent the 'line-height' property to affect the shape of the buttons. The background color and the text color are set here. The sample color that is being used for the background is blue. Although the text color was set to 'black', when the user hover the button the text color will change to another color. The text inside the buttons will be centered. **If the text is too long for the button the 'width' property can be changed.** Other properties were defined to improve the look of the button such as 'border-radius'.

    .boxshadow input[type=submit], .boxshadow button, .boxshadow ul.menu a{
      box-shadow: -1px 1px 6px rgba(0,0,0,0.5), 2px -2px 2px 0px rgba(0,0,0,0.5) inset, -2px 2px 2px 0px rgba(255,255,255,0.5) inset;
    }
    .no-boxshadow input[type=submit], .no-boxshadow button, .no-boxshadow ul.menu a{
      border-top: solid 1px #8888FA;
      border-left: solid 1px #0000A1;
      border-right: solid 1px #8888FA;
      border-bottom: solid 1px #0000A1;
    }

{% include image src="/files/styles/original/public/button%20shadows.jpg" alt="Button shadows (Workaround, CSS3)" description="If box-shadow is not available use borders with similar colors instead of shadows." %}

Two transparent inner shadows will be used in order to improve the look of the buttons. A light upper shadow and a dark lower shadow. Those shadows will improve drastically the look of the buttons adding a convex effect. If the box-shadow feature is not available, a fallback must be set. **Replacing those shadows with two borders with similar colors is a good idea. Make sure those colors are similar to the color of the shadows mixed with background color of the button.**

    .boxshadow input[type=submit]:active, .boxshadow button:active, .boxshadow ul.menu a:active{
      line-height:2em;
      box-shadow: 0px 0px 0px rgba(0,0,0,0.6), -2px 2px 2px 0px rgba(0,0,0,0.5) inset, 2px -2px 2px 0px rgba(255,255,255,0.5) inset;
    }
    .no-boxshadow input[type=submit]:active, .no-boxshadow button:active, .no-boxshadow ul.menu a:active{
      border-top: solid 1px #0000A1;
      border-left: solid 1px #8888FA;
      border-right: solid 1px #0000A1;
      border-bottom: solid 1px #8888FA;
    }
    input[type=submit]:hover, button:hover, ul.menu a:hover{
      color:#C55353;
    }

{% include image src="/files/styles/original/public/active%20button.jpg" alt="Active button (Shadows, CSS3)" %}

This is where the behavior of the buttons is defined. The text color of the buttons will change on hover as well as its inner shadow and the 'line-height' property will change on click. By the time the user clicks the inner shadows will change to the opposite making the buttons look concave. The outer shadow will disappear intensifying the sensation that the button was pressed. The 'line-height' property will increase in order to make the text look lower than usual. The 'height' property of the buttons was previously set, therefore, modifying the 'line-height' property will not affect the shape of the buttons. **Unfortunately, changing the 'line-height' property of the button elements on Firefox may have no effect.**

    .boxshadow input[type=submit], .boxshadow button{
      border:0;
    }/*end buttons style*/

The borders of the button elements are overriden in order to give all the buttons the same look.

    /*begin button transition*/
    .csstransitions.boxshadow input[type=submit], .csstransitions.boxshadow button, .csstransitions.boxshadow ul.menu a{
      transition: color 0.5s, line-height 0.3s, box-shadow 0.3s;
    }/*end button transition*/

This is optional, we can define a transition for all the changing properties. So all the shadows, the text color and the 'line-height' property will change gradually. The shadow is being defined in the transition twice to support browsers that only support 'box-shadow' with prefixes.

## Creating the menus

Once the buttons are completed we can make the menus. A menu is a list with a link in each of its items. In order to set a submenu you must put a list next to the link in the given item of the menu. This structure is needed in order to make the menu work. **This tutorial was intended to make a menu only up to three level. This is how the menu structure should look like.**

    <ul class="menu">
      <li>
        <a href="/yourURL" title="Parent menu option 1">Parent menu option 1</a>
      </li>
      <li>
        <a href="/yourURL" title="Parent menu option 2">Parent menu option 2</a>
        <ul>
          <li>
            <a href="/yourURL" title="Second level submenu option 1">Second level submenu option 1</a>
          </li>
          <li>
            <a href="/yourURL" title="Second level submenu option 2">Second level submenu option 2</a>
            <ul>
              <li>
                <a href="/yourURL" title="Third level submenu option 1">Third level submenu option 1</a>
              </li>
              <li>
                <a href="/yourURL" title="Third level submenu option 2">Third level submenu option 2</a>
              </li>
              ...
	      <li>
                <a href="/yourURL" title="Third level submenu option N">Third level submenu option N</a>
              </li>
            </ul>
          </li>
          ...
          <li>
            <a href="/yourURL" title="Second level submenu option N">Second level submenu option N</a>
          </li>
        </ul>
      </li>
      ...
      <li>
        <a href="/yourURL" title="Parent menu option N">Parent menu option N</a>
      </li>
    </ul>

As you can see, **each final option should have a class attribute called "last". If it is not possible for you, you can leave to set the attribute if you replace the selector '.last' with the pseudo-element selector ':last-child'. Be aware that that pseudo-element selector only works on CSS3.** Once the menu structure is done, the behavior of the menus must be defined. Remember that the buttons are defined already, so the code bellow is just to define the behavior of the menus and its options.

    /*begin menus*/
    ul.menu, ul.menu li{
      display:block;
      position:relative;
      padding:0 !important;
      margin-bottom:0 !important;
      margin-left:0 !important;
      list-style:none !important;
      list-style-type:none !important;
      list-style-image:none !important;
      list-style-position:outside !important;
      float:none;
    }/*end menus: all menus are defined here*/

This is the code that will define all the menus and its options. Part of these properties will affect the submenus as well. All the properties that could affect the look of the menus were overridden. The 'position' property of each menu option was set to 'relative'. A menu option may have a button (a link element) and, possibly, a menu (a submenu we would say). The 'position' property of all the submenus is set to 'absolute', therefore, the submenu will be enclosed in the menu option helping the positioning of the submenu.

    /*begin menu options*/
    ul.menu li{
      margin-right:2em;
      float:left;
    }
      ul.menu li.last{
      margin-right:0px;
    }/*end menu options*/

The 'left' and 'margin-right' properties were set to place each menu option right after another with a margin at the right of each option excepting for the last one, however, the submenu options can not have these properties and they must be replaced or disabled.

    /*begin all submenus*/
    ul.menu ul{
      display:none;
    }/*end all submenus*/

This is a definition for all the submenus. It is just hiding all the submenus by default.

    /*begin 2 level submenu*/
    ul.menu li:hover ul, ul.menu li ul li:hover ul{
      display:block;
      animation:fade 3s;
      animation-play-state:running;
    }
    ul.menu ul{
      position:absolute;
      top:1.8em;
    }
    ul.menu li ul li{
      clear:both;
    }/*end 2 level submenu*/

{% include image src="/files/styles/original/public/secon%20level%20submenu%20positioning.jpg" alt="Second level submenu positioning (CSS3)" %}

The second level submenus are defined here. The third level submenus will inherit part of these properties. **This is where we set the animation to the submenus each time the user hover the option of the parent menu.** The 'animation' property has a time of 3 seconds. This value can be changed as desired. nowadays, all the browsers require a prefix to parse the 'animation' property. The 'animation-play-state' property was set in order to make sure it plays the animation each time the user hover the menu option. The second level submenus will be placed at the bottom of the parent menu. Therefore, the 'top' property is set to 1.8em. The same value of the height of each button. The 'position' property was set to 'absolute' in order to place the submenus properly. **Remember that the submenu is in the menu option so the submenu will be positioned with regard to the menu option position.** Each submenu option has a 'clear' property to disable the effect of the 'left' property inherited from the parent menu options.

    /*begin 3 level submenu*/
      ul.menu li:hover ul ul{
      display:none;
    }
    ul.menu li ul li:hover ul{
      display:block;
    }
    ul.menu ul ul{
      position:absolute;
      left:100%;
      top:0;
    }/*end 3 level submenu*/

{% include image src="/files/styles/original/public/third%20level%20submenu%20positioning.jpg" alt="Third level submenu positioning" %}

The third level submenus must not appear if the second level submenu does, so it is necessary to make sure it will not appear in that situation by setting the 'display' property with the proper selectors. The third level submenu will be placed depending on the position of the second level submenu option. It is necessary to override the inherited position by setting the 'top' property to 0. the third level submenus must be placed to the right of the menu option, so the 'left' property of the submenu is set to 100%.

    /*begin main menu animation*/
    @keyframes fade
    {
    0%   {opacity:0;}
    100% {opacity:1;}
    }
    @-o-keyframes fade
    {
    0%   {opacity:0;}
    100% {opacity:1;}
    }
    @-moz-keyframes fade
    {
    0%   {opacity:0;}
    100% {opacity:1;}
    }
    @-webkit-keyframes fade
    {
    0%   {opacity:0;}
    100% {opacity:1;}
    }/*end main menu animation*/

Lastly, this is the definition of the animation itself. It is just a simple fade effect making appear an object. In case that you want to modify the animation. Remember that only the submenus have the animation not its options.
Once you apply all the code above the result would be a great menu with a fade effect. **Remember that this was made with CSS3 so unsupported browsers would only display the menu, not the effects**. You can customize the animation to display the menu another way else ... and please, if you apply this tutorial **remember to test**.
