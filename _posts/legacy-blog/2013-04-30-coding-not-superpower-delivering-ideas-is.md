---
slug: coding-not-superpower-delivering-ideas-is
title: Coding is not a superpower, delivering ideas is
summary: Is it true that programming is a superpower?. in this article we&#39;re going to discuss the reasons why I totally disagree with this.
imgpath: /files/supercoder_0.jpg
imgdescription: "[Image](http://commons.wikimedia.org/wiki/File%3APlaceholder_male_superhero_c.png) by Vegas Bleeds Neon (modified)"
imgwidth: 480
imgheight: 213
tags: [article]
---
Have you ever dreamed about controlling someone else's life through computers?. In this Web 2.0 era almost everything is being controlled by computers, however, **there are some people who think about technology as if it was the holy spirit thinking that they would become god itself or the definitive superhero if they ever control it somehow** (like in [Watch Dogs](https://www.youtube.com/watch?v=hmmMk5Wb3YU])). For an unexpected reason this is especially related with programming (for example: thanks to everything people think about "hackers"). Are you this kind of guy?. Programming is not a superpower especially because, respecting to me, computer science involves many other things additionally of programming. However, **knowing how to program along with many other things may enable to create a product/business with a relatively easier**. Besides, you can start by yourself if you want; **but if you do not have an objective, plan, dream or idea it is really hard that you achieve something like that**. This just demonstrates that **programming is not a superpower unless you have an idea to develop. Otherwise, is useless, useless like a gun without bullets and a objective to shoot at**.
 
## What is programming
 
I would like to mention programming's definition and where it comes from. Paraphrasing its definition in my own words; it means to specify a set of algorithms (instructions) to a computer in order to solve a specific problem/challenge/goal. Programming is a part of the construction process of software development. Therefore programming can never be applied alone because it is a part of something else. In other words, this means that it comes along with some other actions and activities that must be performed as phases in a specific order. These phases are explained and applied throughout all the computer science career at universities (and applied in the whole professional life of every IT professional);

> This is only a general guideline of the software development process paraphrased by me. The processes of software development may be described differently according to other book authors.

``Analysis -> Design -> Build -> Test -> Deploy``
 
Whether you want it or not you are going to apply all these steps even without you know it. You are going find out what the problem/challenge is (Analysis). Then you are going to find out how to solve/accomplish it (Design). Then you are going to programing it (Build). Then you are going to test it (Test) and very likely to use it (Deploy). All this is going to happen everytime you try to program something. Of course it is highly encouraged that you do this purposely looking forward to improve each process. with all this being said, I would suggest you to call software development a superpower instead of programming solely.
Despite all these activities must be performed in a specific order; they can be performed by different people. There are cases of software development where programmers are supplied of description documents such as flowcharts, UML diagrams and similar being forced to program something concise. Some other activities as debugging or code porting, theoretically, do not rely that much on a great analysis. (In my opinion) These are the nearest situations where programming can be used alone and this is far for being a superpower.
 
## Programming myths
 
By time, many conceptions and beliefs about programming were created. For example; a long time ago people thought that programmers were a kind of artist (implying indirectly that it does not matter if the code is not understood. something that I am not agree with). Not only these conceptions and beliefs were created but also cultures, in fact, programming is surrounded by cultures (such as the hacker culture). Although some of these cultures, beliefs and conceptions have a really solid base. Some other of them are not and are being confused and misinterpreted. There are several myths about programming (regarding the subject of this article) that I think they must be clarified.
 
### "it teaches you how to think"
 
Someone once said this.
 
> I think everybody in this country should learn how to program a computer because it teaches you how to think, Steve Jobs.
 
Let us say you have learned some basics and principles about computer programming and you want to make a program to solve a specific problem. In order to make that program you have to solve the problem and then start writing the code, so what is making you think "solving the problem" or "writing the code"? Programming itself does not teach you how to think; it forces you to solve the problem (which is going to teach you how to think) before doing anything else due to the reasons explained above. If not I challenge you to do it otherwise.
 
Besides, you can still program without knowing the problem completely or the system structure and still find a solution. This implies to program while thinking; something that is possible but highly discouraged because the solution is very likely to be a workaround. Worsening everything for the rest of the people involved.
 
Unfortunately some people who believe this is completely true make many mistakes while trying to encourage people on learning programming. The description of the video "what most schools don't teach" says something like this.
 
### "Learn about a new 'superpower' that isn't being taught in 90% of US schools."
 
The subject of this video is to show some testimonials and references that are supposed to encourage on learning programming. The approach of this video is not the right one because is highly focused on success and money and not about other reasons which does not involve money but challenges. If that is not the case then why did Mark Zuckerberg and Bill Gates have to speak?.
 
### "Good programmers get rich"
 
Unfortunately there are many people who still think programmers can make money out of nothing, that they can get money easily merely by being good with computers. That was somewhat true a lot of time before not now. There are plenty of great programmers that are not rich and many of them are being abused. Additionally, there are many employers who are taking job positions offshore in order to cut costs (sometimes more than 80% of the cost). In worst cases these good programmers are replaced by these people. 
Besides of this, when you are doing a project just for money people usually forget about the real objectives and goals, the ideas do not flow and the work quality drops down noticeably. Actually all the people involved will notice that you are doing this just for money and they will act suspiciously forcing you to cover every objective that you promised. so if you are getting rich by doing something that is because you had the chance and you are good in anything that you do.
 
### "Programming is easy"
 
This sentence is somewhat true but you should ask to yourself "is 'solving problems' easy?". Many people who take programming classes for the first time think they can replace a programmer easily with little time. Some other people (employers mostly) think if programming is easy debugging will also be; based on this, many people qualify projects and problems as easy thinking they can be completed or solved fast without really knowing if this is really true. 
 
In my personal opinion, learning software development is not something easy to learn (as everything else in the life); it requires dedication and effort. Software development has several principles to follow and many methodologies of any kind (related with all the activities not just programming). Many projects fail due to bad analysis, interpretation of the requirements or the project management. Additionally, some technologies would require their own principles and methodologies.
 
## The real superpower
 
Programming (or software development should I say) is something great to learn; not because it has something especial by itself, it is because it allows you to deliver ideas relatively easier than other sciences. This neither means that programming is a superpower nor that other fields of engineering or science suck, but the real superpower is to take challenges and make ideas come to life. Then if you think you are a wizard merely by knowing to develop software you should also consider people who make great stuff with electronic, robotic or prototyping (just to mention a few fields that I am interested in).
Besides, a superpower is intended to be an ability that almost no one has which gives you an advantage over the rest of the people. In this respect, these people with "superpowers" with good intentions are usually called "superheroes". Theoretically, Programming can not be considered as a superpower due to a few reasons; it can be learned (and is [somewhat] easy to learn), there is a lot of people who already know how to program, and lastly knowing how to program is not a great advantage itself. However, if we think about "ideas" it would match superpower's definition; it can not be learned, it is pretty weird when a group of people have the same idea at time, and it gives you a great advantage over the rest of the people.
Unfortunately there are a lot of people that are being unmentioned due to the way the world's society interpret facts and events. I saw really great people being mentioned just once while other people (mostly rich) mentioned thousands of times just because they built a company (or companies) and they are rich nowadays. For example, think about the case of Steve jobs and Dennis Ritchie. Steve jobs is still mentioned up to these days while Dennis Ritchie was barely mentioned.

{% include image src="/files/steve-jobs-dennis-ritchie.jpg" alt="Jobs - Ritchie" %}

Although programming provides a direct and relatively easy way to deliver ideas; that does not mean that ideas can not be delivered doing something else. Actually, there are many things that can be done without programming. This is why I personally consider "working on ideas" is a real superpower. You can work on something even without knowing anything. What will happen in those situations is that you will learn along the way. These are real superheroes because they take challenges and make great stuff in order to help people somehow.
 
### Izhar Gafni - Cardboard Bicycle
> "In 2012, Izhar Gafni, a mechanical engineer and cycling enthusiast, unveiled a prototype bicycle made almost entirely out of cardboard in his workshop in Moshav Ahituv. The components, including bike’s frame, wheels, handlebars and saddle, consist of sheets of cardboard folded and glued together. The complete bike weighs 20 pounds (9.1 kg), and is treated to be fireproof and waterproof. Gafni reports that it can support riders up to 220 kilograms (490 lb). It has solid rubber tires made from recycled car tires. Power is transferred from the pedals to the rear wheel with a belt, also made from recycled rubber. Gafni and a business partner plan to mass-produce a bike based on the prototype and retail it for 20 USD, with a unit cost of 9 to 12 USD.", By Wikipedia, The Free Encyclopedia.

### Maasai Richard Turere - Lion Lights
> "'Lion Lights' are flashing lights set up around a perimeter facing outwards; which are used to scare away lions.
> The lion lights were devised by Maasai Richard Turere to prevent night attacks by lions on his family's cattle herd, which was located in Kitengela on the unfenced south side of Nairobi National Park, in Kenya. These types of attacks often lead to the hunting and killing of the lions, which are endangered.", By Wikipedia, The Free Encyclopedia.
 
### Julia Silverman, Jessica Matthews - Soccket
> "SOCCKET is a soccer ball that harnesses and stores energy from play for later use as portable power source in resource-poor areas. It is the flagship product of Uncharted Play, Incorporated.", "Jessica Lin, Julia Silverman, Jessica Matthews, Hemali Thakkar, who were at the time undergraduates at Harvard University, and Aviva Presser, who was a Harvard graduate student at the time, were the inventors listed on the initial patent.", By Wikipedia, The Free Encyclopedia.
 
So if you really want to become a superhero then you should stop thinking like this and start doing great stuff with good intentions (I mean with "great stuff" great ideas to develop). Keep in mind that everything depends on persistency, so before quitting try to think if you really have used all your plans. Remember there is no such a thing as superpowers; only real heroes. People who have the power to change the world with their decisions and actions.

