---
title: "Privacy Policy"
permalink: /privacy-policy/
referred-as: MARP
layout: boxdrawn
no_sidebar: true
---
These are the privacy policies that govern this website, this website is owned and operated by Miguel Ángel Rodríguez, who may be referred as "{{page.referred-as}}" from now on, or may be referred as a plural with the pronouns "we", “us” or "our”, depending on the context.
 
## What information do we collect? 
 
```
We collect information from you when you subscribe to our newsletter or fill out a form.
```
That is no longer true. We do not collect information of any kind at the moment
 
## What do we use your information for? 

Because we do not collect information at the moment we do not have information to use.

## Do we use cookies? 
 
We do not use cookies at the moment.
 
## Do we disclose any information to outside parties? 

No.
This was in a previus version of this document:
``` 
We do not sell, trade, or otherwise transfer to outside parties
 your personally identifiable information. This does not include
trusted third parties who assist us in operating our website,
conducting our business, or servicing you, so long as those parties
agree to keep this information confidential. We may also release
your information when we believe release is appropriate to comply
with the law, enforce our site policies, or protect ours or others
rights, property, or safety. However, non-personally identifiable
visitor information may be provided to other parties for marketing,
advertising, or other uses.
```
We had advertisement, newsletters and third party services for commenting, but that is not longer available in this site at the moment.
 
## Third party links
 
Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.

## Online Privacy Policy Only
 
This online privacy policy applies only to information collected through our website and not to information collected offline.
 
## Terms and Conditions 
 
Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at /terms-conditions
 
## Your Consent 
 
By using our site, you consent to our websites privacy policy.
 
## Changes to our Privacy Policy 
 
If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below.

### 2019/09/15
 
